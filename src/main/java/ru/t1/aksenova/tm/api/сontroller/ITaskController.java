package ru.t1.aksenova.tm.api.сontroller;

import ru.t1.aksenova.tm.model.Task;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

    void showTaskById();

    void showTaskByIndex();

    void showTask(Task task);

    void updateTaskByIndex();

    void updateTaskById();

    void removeTaskById();

    void removeTaskByIndex();

    void startTaskByIndex();

    void startTaskById();

    void completeTaskByIndex();

    void completeTaskById();

    void changeTaskStatusByIndex();

    void changeTaskStatusById();

}
